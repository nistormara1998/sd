import React, {Component} from 'react'
import CaregiverApi from '../api/CaregiverApi';

class CreateCaregiverComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: '',
            name: '',
            dateOfBirth: '',
            gender: '',
            address: '',
            username: '',
            password: ''
        }

        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeDateOfBirthHandler = this.changeDateOfBirthHandler.bind(this);
        this.changeGenderHandler = this.changeGenderHandler.bind(this);
        this.changeAddressHandler = this.changeAddressHandler.bind(this);
        this.changeUsernameHandler = this.changeUsernameHandler.bind(this);
        this.changePasswordHandler = this.changePasswordHandler.bind(this);
        this.saveOrUpdateCaregiver = this.saveOrUpdateCaregiver.bind(this);
    }

    componentDidMount() {

        if (this.state.id === '_add') {
            return
        } else {
            CaregiverApi.getCaregiverById(this.state.id).then((res) => {
                let caregiver = res.data;
                this.setState({
                    id: caregiver.id,
                    name: caregiver.name,
                    dateOfBirth: caregiver.dateOfBirth,
                    gender: caregiver.dateOfBirth,
                    address: caregiver.address,
                    medicalRecord: caregiver.medicalRecord,
                    username: caregiver.username,
                    password: caregiver.password,
                });
            });
        }
    }

    saveOrUpdateCaregiver = (e) => {
        e.preventDefault();
        let caregiver = {
            id: this.state.id,
            name: this.state.name,
            dateOfBirth: this.state.dateOfBirth,
            gender: this.state.gender,
            address: this.state.address,
            medicalRecord: this.state.medicalRecord,
            username: this.state.username,
            password: this.state.password,
        };
        console.log('caregiver => ' + JSON.stringify(caregiver));


        if (window.location.pathname.includes('_add')) {
            CaregiverApi.insertCaregiver(caregiver).then(res => {
                this.props.history.push('/doctor');
            });
        } else {
            let caregiverId = window.location.pathname.substring(15,);
            CaregiverApi.updateCaregiver(caregiver, caregiverId).then(res => {
                this.props.history.push('/doctor');
            });
        }
    }

    changeNameHandler = (event) => {
        this.setState({name: event.target.value});
    }

    changeDateOfBirthHandler = (event) => {
        this.setState({dateOfBirth: event.target.value});
    }

    changeGenderHandler = (event) => {
        this.setState({gender: event.target.value});
    }

    changeAddressHandler = (event) => {
        this.setState({address: event.target.value});
    }


    changeUsernameHandler = (event) => {
        this.setState({username: event.target.value});
    }

    changePasswordHandler = (event) => {
        this.setState({password: event.target.value});
    }

    cancel() {
        this.props.history.push('/doctor');
    }

    getTitle() {
        if (window.location.pathname.includes('_add')) {
            return <h3 className="text-center">Add Caregiver</h3>
        } else {
            return <h3 className="text-center">Update Caregiver</h3>
        }
    }

    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label> Name: </label>
                                        <input placeholder="Name" name="name" className="form-control"
                                               value={this.state.name} onChange={this.changeNameHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Date Of Birth: </label>
                                        <input placeholder="Date Of Birth" name="dateOfBirth" className="form-control"
                                               value={this.state.dateOfBirth} onChange={this.changeDateOfBirthHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Gender: </label>
                                        <input placeholder="Gender" name="gender" className="form-control"
                                               value={this.state.gender} onChange={this.changeGenderHandler}/>
                                    </div>

                                    <div className="form-group">
                                        <label> Address: </label>
                                        <input placeholder="Address" name="address" className="form-control"
                                               value={this.state.address} onChange={this.changeAddressHandler}/>
                                    </div>

                                    <div className="form-group">
                                        <label> Username: </label>
                                        <input placeholder="Username" name="username" className="form-control"
                                               value={this.state.username} onChange={this.changeUsernameHandler}/>
                                    </div>


                                    <div className="form-group">
                                        <label> Password: </label>
                                        <input placeholder="Password" name="password" className="form-control"
                                               value={this.state.password} onChange={this.changePasswordHandler}/>
                                    </div>


                                    <button className="btn btn-success" onClick={this.saveOrUpdateCaregiver}>Save
                                    </button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)}
                                            style={{marginLeft: "10px"}}>Cancel
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default CreateCaregiverComponent