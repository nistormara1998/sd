import React, {Component} from 'react'
import MedicationApi from '../api/MedicationApi';

class CreateMedicationComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: '',
            name: '',
            effects: '',
            dosage: '',
        }

        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeEffectsHandler = this.changeEffectsHandler.bind(this);
        this.changeDosageHandler = this.changeDosageHandler.bind(this);
        this.saveOrUpdateMedication = this.saveOrUpdateMedication.bind(this);
    }

    componentDidMount() {

        if (this.state.id === '_add') {
            return
        } else {
            MedicationApi.getMedicationById(this.state.id).then((res) => {
                let medication = res.data;
                this.setState({
                    id: medication.id,
                    name: medication.name,
                    effects: medication.effects,
                    dosage: medication.dosage,
                });
            });
        }
    }

    saveOrUpdateMedication = (e) => {
        e.preventDefault();
        let medication = {
            id: this.state.id,
            name: this.state.name,
            effects: this.state.effects,
            dosage: this.state.dosage,
        };
        console.log('medication => ' + JSON.stringify(medication));


        if (window.location.pathname.includes('_add')) {
            MedicationApi.insertMedication(medication).then(res => {
                this.props.history.push('/doctor');
            });
        } else {
            let medicationId = window.location.pathname.substring(15,);
            MedicationApi.updateMedication(medication, medicationId).then(res => {
                this.props.history.push('/doctor');
            });
        }
    }

    changeNameHandler = (event) => {
        this.setState({name: event.target.value});
    }

    changeEffectsHandler = (event) => {
        this.setState({effects: event.target.value});
    }

    changeDosageHandler = (event) => {
        this.setState({dosage: event.target.value});
    }

    cancel() {
        this.props.history.push('/doctor');
    }

    getTitle() {
        if (window.location.pathname.includes('_add')) {
            return <h3 className="text-center">Add Medication</h3>
        } else {
            return <h3 className="text-center">Update Medication</h3>
        }
    }

    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label> Name: </label>
                                        <input placeholder="Name" name="name" className="form-control"
                                               value={this.state.name} onChange={this.changeNameHandler}/>
                                    </div>

                                    <div className="form-group">
                                        <label> Effects: </label>
                                        <input placeholder="Effects" name="effects" className="form-control"
                                               value={this.state.effects} onChange={this.changeEffectsHandler}/>
                                    </div>


                                    <div className="form-group">
                                        <label> Dosage: </label>
                                        <input placeholder="Dosage" name="dosage" className="form-control"
                                               value={this.state.dosage} onChange={this.changeDosageHandler}/>
                                    </div>


                                    <button className="btn btn-success" onClick={this.saveOrUpdateMedication}>Save
                                    </button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)}
                                            style={{marginLeft: "10px"}}>Cancel
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default CreateMedicationComponent