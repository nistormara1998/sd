import React, { Component } from 'react'
import PatientApi from '../api/PatientApi'

class ViewPatientComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            patient: {}
        }
    }

    componentDidMount(){
        PatientApi.getPatientById(this.state.id).then( res => {
            this.setState({patient: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Patient Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Patient Name: </label>
                            <div> { this.state.patient.name }</div>
                        </div>
                        <div className = "row">
                            <label> Patient DoB: </label>
                            <div> { this.state.patient.dateOfBirth }</div>
                        </div>
                        <div className = "row">
                            <label> Patient Gender: </label>
                            <div> { this.state.patient.gender }</div>
                        </div>

                        <div className = "row">
                            <label> Patient Address: </label>
                            <div> { this.state.patient.address }</div>
                        </div>
                    </div>

                    <div className = "row">
                        <label> Patient Medical Record: </label>
                        <div> { this.state.patient.medicalRecord }</div>
                    </div>

                    <div className = "row">
                        <label> Patient username: </label>
                        <div> { this.state.patient.username }</div>
                    </div>

                    <div className = "row">
                        <label> Patient password: </label>
                        <div> { this.state.patient.password }</div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewPatientComponent