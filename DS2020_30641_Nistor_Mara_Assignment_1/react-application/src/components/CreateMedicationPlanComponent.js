import React, {Component} from 'react'
import MedicationPlanApi from "../api/MedicationPlanApi";
import MedicationApi from "../api/MedicationApi";
import PatientApi from "../api/PatientApi";

class CreateMedicationPlanComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: '',
            medicationId: '',
            patientId: '',
            intake: '',
            period: '',
        }

        this.changeIdHandler = this.changeIdHandler.bind(this);
        this.changeMedicationIdHandler = this.changeMedicationIdHandler.bind(this);
        this.changePatientIdHandler = this.changePatientIdHandler.bind(this);
        this.changeIntakeHandler = this.changeIntakeHandler.bind(this);
        this.changePeriodHandler = this.changePeriodHandler.bind(this);
        this.saveMedicationPlan = this.saveMedicationPlan.bind(this);
    }

    componentDidMount() {

        MedicationPlanApi.getMedicationPlanById(this.state.id).then((res) => {
            let medicationPlan = res.data;
            this.setState({
                id: medicationPlan.id,
                medicationId: medicationPlan.medicationId,
                medicationPlan: medicationPlan.patientId,
                intake: medicationPlan.intake,
                period: medicationPlan.period
            });
        });


    }

    saveMedicationPlan = (e) => {
        e.preventDefault();
        let medicationPlan = {
            medicationId: this.state.medicationId,
            patientId: this.state.patientId,
            intake: this.state.intake,
            period: this.state.period,
        };
        console.log('medicationPlan => ' + JSON.stringify(medicationPlan));


        MedicationPlanApi.insertMedicationPlan(medicationPlan).then(res => {
            this.props.history.push('/doctor');
        });

        let medicationPlanId = window.location.pathname.substring(20,);

        MedicationApi.addToMedicationPlan(medicationPlan.medicationId,medicationPlanId).then(res => {
            this.props.history.push('/doctor');
        });

        MedicationPlanApi.assignToPatient(medicationPlan.patientId,medicationPlanId).then(res => {
            this.props.history.push('/doctor');
        });
    }

    changeIdHandler = (event) => {
        this.setState({id: event.target.value});
    }

    changeMedicationIdHandler = (event) => {
        this.setState({medicationId: event.target.value});
    }

    changePatientIdHandler = (event) => {
        this.setState({patientId: event.target.value});
    }

    changeIntakeHandler = (event) => {
        this.setState({intake: event.target.value});
    }

    changePeriodHandler = (event) => {
        this.setState({period: event.target.value});
    }

    cancel() {
        this.props.history.push('/doctor');
    }

    getTitle() {
        if (window.location.pathname.includes('_add')) {
            return <h3 className="text-center">Add Caregiver</h3>
        } else {
            return <h3 className="text-center">Update Caregiver</h3>
        }
    }

    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label> Medication Id: </label>
                                        <input placeholder="Medication Id" name="medicationId" className="form-control"
                                               value={this.state.medicationId}
                                               onChange={this.changeMedicationIdHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Patient Id: </label>
                                        <input placeholder="Patient Id" name="patientId" className="form-control"
                                               value={this.state.patientId} onChange={this.changePatientIdHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Intake: </label>
                                        <input placeholder="Intake" name="intake" className="form-control"
                                               value={this.state.intake} onChange={this.changeIntakeHandler}/>
                                    </div>

                                    <div className="form-group">
                                        <label> Period: </label>
                                        <input placeholder="Period" name="period" className="form-control"
                                               value={this.state.period} onChange={this.changePeriodHandler}/>
                                    </div>

                                    <button className="btn btn-success" onClick={this.saveMedicationPlan}>Save
                                    </button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)}
                                            style={{marginLeft: "10px"}}>Cancel
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default CreateMedicationPlanComponent