import React, {Component} from 'react';
import '../App.css';
import PatientApi from '../api/PatientApi';
import CaregiverApi from '../api/CaregiverApi';

class LoginComponent extends Component {

    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            error: '',
            patients: [],
            caregivers: [],
            doctors: []
        };

        this.changeUsernameHandler = this.changeUsernameHandler.bind(this);
        this.changePasswordHandler = this.changePasswordHandler.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.dismissError = this.dismissError.bind(this);
    }

    dismissError() {
        this.setState({error: ''});
    }

    handleLogin(evt) {
        evt.preventDefault();

        this.setState({error: false});

        let okPatient = 0;
        let okCaregiver = 0;
        let okDoctor = 0;
        let patientLogin;

        PatientApi.getPatients().then((res) => {
            this.setState({patients: res.data});
        });

        CaregiverApi.getCaregivers().then((res) => {
            this.setState({caregivers: res.data});
        });


        if ((this.state.username === 'doctor' && this.state.password === 'sunl1ght')) {
            okDoctor = 1;
        }

        for (let i = 0; i < this.state.patients.length; i++) {
            if ((this.state.patients[i].username === this.state.username && this.state.patients[i].password === this.state.password)) {
                patientLogin = this.state.patients[i];
                okPatient = 1;
            }

        }

        for (let i = 0; i < this.state.caregivers.length; i++) {
            if ((this.state.caregivers[i].username === this.state.username && this.state.caregivers[i].password === this.state.password)) {
                okCaregiver = 1;
            }

        }

        //store.set('loggedIn', true);
        if(okDoctor === 1){
            this.props.history.push('/doctor');
        }
        else if(okCaregiver === 1){
            this.props.history.push("/caregiver");
        }
        else if(okPatient === 1){
            this.props.history.push(`/view-patient/${patientLogin.id}`);
        }
        else{
            this.setState({error: true});
            alert("Username or password is incorrect!");
        }

    }

    viewPatient(id){
        this.props.history.push(`/view-patient/${id}`);
    }

    changeUsernameHandler = (event) => {
        this.setState({username: event.target.value});
    }

    changePasswordHandler = (event) => {
        this.setState({password: event.target.value});
    }

    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            {
                                <h3 className="text-center">Login</h3>
                            }
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label> Username: </label>
                                        <input placeholder="Username" name="username" className="form-control"
                                               value={this.state.username} onChange={this.changeUsernameHandler}/>
                                    </div>


                                    <div className="form-group">
                                        <label> Password: </label>
                                        <input placeholder="Password" name="password" className="form-control"
                                               value={this.state.password} onChange={this.changePasswordHandler}/>
                                    </div>


                                    <button className="btn btn-success" onClick={this.handleLogin}>Login</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )

    }

}

export default LoginComponent