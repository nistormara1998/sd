import React, { Component } from 'react'
import PatientApi from '../api/PatientApi'
import CaregiverApi from '../api/CaregiverApi'
import MedicationApi from '../api/MedicationApi'

class DoctorPageComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            patients: [],
            caregivers: [],
            medications:[]
        }
        this.addPatient = this.addPatient.bind(this);
        this.editPatient = this.editPatient.bind(this);
        this.deletePatient = this.deletePatient.bind(this);

        this.addCaregiver = this.addCaregiver.bind(this);
        this.editCaregiver = this.editCaregiver.bind(this);
        this.deleteCaregiver = this.deleteCaregiver.bind(this);

        this.addMedication = this.addMedication.bind(this);
        this.editMedication = this.editMedication.bind(this);
        this.deleteMedication = this.deleteMedication.bind(this);

        this.addMedicationPlan = this.addMedicationPlan.bind(this);

    }

    deletePatient(id){
        PatientApi.deletePatient(id).then( res => {
            this.setState({patients: this.state.patients.filter(patient => patient.id !== id)});
        });
    }

    viewPatient(id){
        this.props.history.push(`/view-patient/${id}`);
    }

    editPatient(id){
        this.props.history.push(`/add-patient/${id}`);
    }

    deleteMedication(id){
        MedicationApi.deleteMedication(id).then( res => {
            this.setState({medications: this.state.medications.filter(medication => medication.id !== id)});
        });
    }

    viewMedication(id){
        this.props.history.push(`/view-medication/${id}`);
    }

    editMedication(id){
        this.props.history.push(`/add-medication/${id}`);
    }

    viewCaregiver(id){
        this.props.history.push(`/view-caregiver/${id}`);
    }

    addMedicationPlan(id){
        this.props.history.push(`/add-medicationPlan/${id}`);
    }




    deleteCaregiver(id){
        CaregiverApi.deleteCaregiver(id).then( res => {
            this.setState({caregivers: this.state.caregivers.filter(caregiver => caregiver.id !== id)});
        });
    }


    editCaregiver(id){
        this.props.history.push(`/add-caregiver/${id}`);
    }

    componentDidMount(){
        PatientApi.getPatients().then((res) => {
            this.setState({ patients: res.data});
        });

        CaregiverApi.getCaregivers().then((res) => {
            this.setState({ caregivers: res.data});
        });

        MedicationApi.getMedications().then((res) => {
            this.setState({ medications: res.data});
        });

    }

    addPatient(){
        this.props.history.push('/add-patient/_add');
    }

    addCaregiver(){
        this.props.history.push('/add-caregiver/_add');
    }

    addMedication(){
        this.props.history.push('/add-medication/_add');
    }


    render() {
        return (
            <div>
                <h2 className="text-center">Patient List</h2>
                <div className = "row">
                    <button className="btn btn-primary" onClick={this.addPatient}> Add Patient</button>
                </div>
                <br></br>
                <div className = "row">
                    <table className = "table table-striped table-bordered">

                        <thead>
                        <tr>
                            <th> Id</th>
                            <th> Name</th>
                            <th> Username</th>
                            <th> Password</th>
                            <th> Date of Birth</th>
                            <th> Gender</th>
                            <th> Address</th>
                            <th> MedicalRecord</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.patients.map(
                                patient =>
                                    <tr key = {patient.id}>
                                        <td> { patient.id} </td>
                                        <td> { patient.name} </td>
                                        <td> { patient.username} </td>
                                        <td> { patient.password} </td>
                                        <td> {patient.dateOfBirth}</td>
                                        <td> {patient.gender}</td>
                                        <td> {patient.address}</td>
                                        <td> {patient.medicalRecord}</td>
                                        <td>
                                            <button onClick={ () => this.editPatient(patient.id)} className="btn btn-info">Update </button>
                                            <button style={{marginLeft: "10px"}} onClick={ () => this.deletePatient(patient.id)} className="btn btn-danger">Delete </button>
                                            <button style={{marginLeft: "10px"}} onClick={ () => this.viewPatient(patient.id)} className="btn btn-info">View </button>
                                            <button style={{marginLeft: "5%"}} onClick={ () => this.addMedicationPlan(patient.id)} className="btn btn-dark">Add Medication Plan </button>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>

                </div>

                <h2 className="text-center">Caregiver List</h2>
                <div className = "row">
                    <button className="btn btn-primary" onClick={this.addCaregiver}> Add Caregiver</button>
                </div>
                <br></br>
                <div className = "row">
                    <table className = "table table-striped table-bordered">

                        <thead>
                        <tr>
                            <th> Id</th>
                            <th> Name</th>
                            <th> Username</th>
                            <th> Password</th>
                            <th> Date of Birth</th>
                            <th> Gender</th>
                            <th> Address</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.caregivers.map(
                                caregiver =>
                                    <tr key = {caregiver.id}>
                                        <td> { caregiver.id} </td>
                                        <td> { caregiver.name} </td>
                                        <td> { caregiver.username} </td>
                                        <td> { caregiver.password} </td>
                                        <td> {caregiver.dateOfBirth}</td>
                                        <td> {caregiver.gender}</td>
                                        <td> {caregiver.address}</td>
                                        <td>
                                            <button onClick={ () => this.editCaregiver(caregiver.id)} className="btn btn-info">Update </button>
                                            <button style={{marginLeft: "10px"}} onClick={ () => this.deleteCaregiver(caregiver.id)} className="btn btn-danger">Delete </button>
                                            <button style={{marginLeft: "10px"}} onClick={ () => this.viewCaregiver(caregiver.id)} className="btn btn-info">View </button>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>

                <h2 className="text-center">Medication List</h2>
                <div className = "row">
                    <button className="btn btn-primary" onClick={this.addMedication}> Add Medication</button>
                </div>
                <br></br>
                <div className = "row">
                    <table className = "table table-striped table-bordered">

                        <thead>
                        <tr>
                            <th> Id</th>
                            <th> Name</th>
                            <th> Effects</th>
                            <th> Dosage</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.medications.map(
                                medication =>
                                    <tr key = {medication.id}>
                                        <td> { medication.id} </td>
                                        <td> { medication.name} </td>
                                        <td> { medication.effects} </td>
                                        <td> { medication.dosage} </td>
                                        <td>
                                            <button onClick={ () => this.editMedication(medication.id)} className="btn btn-info">Update </button>
                                            <button style={{marginLeft: "10px"}} onClick={ () => this.deleteMedication(medication.id)} className="btn btn-danger">Delete </button>
                                            <button style={{marginLeft: "10px"}} onClick={ () => this.viewMedication(medication.id)} className="btn btn-info">View </button>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>

            </div>

        )
    }
}

export default DoctorPageComponent