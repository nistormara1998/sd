import React, { Component } from 'react'

class HeaderComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

        this.handleLogout = this.handleLogout.bind(this);
    }


    handleLogout(){

        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                        <div><a href="https://javaguides.net" className="navbar-brand"> Medical Monitoring Platform</a></div>

                        <a href="/" class="btn btn-danger" style={{marginLeft:"1600px"}}> Logout </a>
                    </nav>
                </header>
            </div>
        )
    }
}

export default HeaderComponent