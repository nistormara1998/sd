import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import CreatePatientComponent from './components/CreatePatientComponent';
import ViewPatientComponent from './components/ViewPatientComponent';
import DoctorPageComponent from "./components/DoctorPageComponent";
import CreateCaregiverComponent from "./components/CreateCaregiverComponent";
import CreateMedicationComponent from "./components/CreateMedicationComponent";
import LoginComponent from "./components/LoginComponent";
import CreateMedicationPlanComponent from "./components/CreateMedicationPlanComponent";

function App() {
    return (
        <div>
            <Router>
                <div>
                <HeaderComponent/>
                <div className="container">
                    <Switch>
                        <Route path="/" exact component={LoginComponent}></Route>
                        <Route path="/login" exact component={LoginComponent}></Route>
                        <Route path="/doctor" component={DoctorPageComponent}></Route>
                        <Route path="/add-patient/:id" component={CreatePatientComponent}></Route>
                        <Route path="/view-patient/:id" component={ViewPatientComponent}></Route>
                        <Route path="/view-medication/:id" component={ViewPatientComponent}></Route>
                        <Route path="/view-caregiver/:id" component={ViewPatientComponent}></Route>
                        <Route path="/add-caregiver/:id" component={CreateCaregiverComponent}></Route>
                        <Route path="/add-medication/:id" component={CreateMedicationComponent}></Route>
                        <Route path="/add-medicationPlan/:id" component={CreateMedicationPlanComponent}></Route>

                        {/* <Route path = "/update-employee/:id" component = {UpdateEmployeeComponent}></Route> */}
                    </Switch>
                </div>
                <FooterComponent/>
                </div>
            </Router>
        </div>

    );
}

export default App;