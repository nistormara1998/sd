import axios from 'axios';
import {HOST} from "../commons/hosts";


const endpoint = {
    medication: '/rest/medication',
    delete: 'delete',
    insert: '/insert',
    update: '/update',
    addToMedicationPlan: "/addToMedicationList/"
};

class MedicationApi {

    getMedications() {
        return axios.get(HOST.backend_api + endpoint.medication);
    }

    getMedicationById(medicationId) {
        return axios.get(HOST.backend_api + endpoint.medication + '/' + medicationId);
    }

    deleteMedication(medicationId) {
        return axios.delete(HOST.backend_api + endpoint.medication + '/' + endpoint.delete + '/' + medicationId);
    }

    insertMedication(medication) {
        return axios.post(HOST.backend_api + endpoint.medication + endpoint.insert, medication);
    }

    updateMedication(medication, medicationId) {
        return axios.put(HOST.backend_api + endpoint.medication + endpoint.update + '/' + medicationId, medication)
    }

    addToMedicationPlan(medicationId, medicationPlanId) {
        return axios.put(HOST.backend_api + endpoint.medication + endpoint.addToMedicationPlan +  medicationId + "/" + medicationPlanId)
    }


}

export default new MedicationApi();