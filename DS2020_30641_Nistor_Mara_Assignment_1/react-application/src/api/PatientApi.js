import axios from 'axios';
import {HOST} from "../commons/hosts";


const endpoint = {
    patient: '/rest/patient',
    delete: 'delete',
    insert: '/insert',
    update: '/update',
    addToCaregiver: '/addToCaregiver/'
};

class PatientApi {

//e ok
    getPatients() {
        return axios.get(HOST.backend_api + endpoint.patient);
    }
//e ok
    getPatientById(patientId) {
        return axios.get(HOST.backend_api + endpoint.patient + '/' + patientId);
    }
// e ok
    deletePatient(patientId) {
        return axios.delete(HOST.backend_api + endpoint.patient + '/' + endpoint.delete + '/' + patientId);
    }

    insertPatient(patient) {
        return axios.post(HOST.backend_api + endpoint.patient + endpoint.insert, patient);
    }

    updatePatient(patient, patientId) {
        return axios.put(HOST.backend_api + endpoint.patient + endpoint.update + '/' + patientId, patient)
    }

    assignCaregiver(patientId, caregiverId) {
        return axios.put(HOST.backend_api + endpoint.patient + endpoint.addToCaregiver + patientId + "/" + caregiverId)
    }


}

export default new PatientApi();