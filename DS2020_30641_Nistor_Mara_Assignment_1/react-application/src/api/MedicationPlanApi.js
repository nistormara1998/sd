import axios from 'axios';
import {HOST} from "../commons/hosts";


const endpoint = {
    medicationPlan: '/rest/medicationplan',
    delete: 'delete',
    insert: '/insert',
    update: '/update',
    assignToPatient: "/addToPatient/"
};

class MedicationPlanApi {

    getMedicationPlans() {
        return axios.get(HOST.backend_api + endpoint.medicationPlan);
    }

    getMedicationPlanById(medicationPlanId) {
        return axios.get(HOST.backend_api + endpoint.medication + '/' + medicationPlanId);
    }

    deleteMedicationPlan(medicationPlanId) {
        return axios.delete(HOST.backend_api + endpoint.medicationPlan + '/' + endpoint.delete + '/' + medicationPlanId);
    }

    insertMedicationPlan(medicationPlan) {
        return axios.post(HOST.backend_api + endpoint.medicationPlan + endpoint.insert, medicationPlan);
    }


    assignToPatient(patientId, medicationPlanId) {
        return axios.put(HOST.backend_api + endpoint.medicationPlan + endpoint.assignToPatient + patientId + "/" + medicationPlanId);
    }




}

export default new MedicationPlanApi();