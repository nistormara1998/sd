import axios from 'axios';
import {HOST} from "../commons/hosts";


const endpoint = {
    caregiver: '/rest/caregiver',
    delete: 'delete',
    insert: '/insert',
    update: '/update'
};

class CaregiverApi {

//e ok
    getCaregivers() {
        return axios.get(HOST.backend_api + endpoint.caregiver);
    }
//e ok
    getCaregiverById(caregiverId) {
        return axios.get(HOST.backend_api + endpoint.caregiver + '/' + caregiverId);
    }
// e ok
    deleteCaregiver(caregiverId) {
        return axios.delete(HOST.backend_api + endpoint.caregiver + '/' + endpoint.delete + '/' + caregiverId);
    }

    insertCaregiver(caregiver) {
        return axios.post(HOST.backend_api + endpoint.caregiver + endpoint.insert, caregiver);
    }

    updateCaregiver(caregiver, caregiverId) {
        return axios.put(HOST.backend_api + endpoint.caregiver + endpoint.update + '/' + caregiverId, caregiver)
    }


}

export default new CaregiverApi();