package com.example.springapplication.services;

import com.example.springapplication.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.example.springapplication.dtos.MedicationDTO;
import com.example.springapplication.dtos.builders.MedicationBuilder;
import com.example.springapplication.entities.Medication;
import com.example.springapplication.entities.MedicationPlan;
import com.example.springapplication.repositories.MedicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public Medication assignMedicationToMedicationPlan(MedicationPlan medicationPlan, Medication medication){
        medication.setMedicationPlan(medicationPlan);
        medicationRepository.save(medication);
        LOGGER.debug("Medication with name {} was assigned to a medication plan", medication.getName());
        return medication;
    }

    public List<MedicationDTO> findMedication() {
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(Integer id) {
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.toMedicationDTO(medicationOptional.get());
    }

    public Integer insert(MedicationDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    public Integer update(Integer id, MedicationDTO medicationDTO) {

        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        Medication medication = medicationOptional.get();
        medication.setName(medicationDTO.getName());
        medication.setDosage(medicationDTO.getDosage());
        medication.setEffects(medicationDTO.getEffects());
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was updated in db", medication.getId());
        return medication.getId();

    }

    public void delete(Integer medicationId) {
        medicationRepository.deleteById(medicationId);
    }

}
