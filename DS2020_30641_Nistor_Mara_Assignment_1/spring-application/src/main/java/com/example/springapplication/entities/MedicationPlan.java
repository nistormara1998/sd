package com.example.springapplication.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medicationplan")
public class MedicationPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @OneToMany(mappedBy = "medicationPlan",
            fetch = FetchType.EAGER)
    private Set<Medication> medicationList = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    private Patient patient;


    @Column(name = "intake", nullable = false)
    private String intake;

    @Column(name = "period", nullable = false)
    private String period;

    public MedicationPlan() {

    }

    public MedicationPlan(Integer id, Set<Medication> medicationList, String intake, String period) {
        this.id = id;
        this.medicationList = medicationList;
        this.intake = intake;
        this.period = period;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(Set<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }


    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
