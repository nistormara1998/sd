package com.example.springapplication.services;

import com.example.springapplication.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.example.springapplication.dtos.CaregiverDTO;
import com.example.springapplication.dtos.CaregiverDetailsDTO;
import com.example.springapplication.dtos.builders.CaregiverBuilder;
import com.example.springapplication.entities.Caregiver;
import com.example.springapplication.entities.Patient;
import com.example.springapplication.repositories.CaregiverRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public List<CaregiverDetailsDTO> findCaregivers() {
        List<Caregiver> caregivers = caregiverRepository.findAll();
        return caregivers.stream()
                .map(CaregiverBuilder::toCaregiverDetailsDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDetailsDTO findCaregiverById(Integer id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDetailsDTO(caregiverOptional.get());
    }

    public Integer insert(CaregiverDetailsDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public Integer update(Integer id, CaregiverDetailsDTO caregiverDTO) {

        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        Caregiver caregiver = caregiverOptional.get();
        caregiver.setUsername(caregiverDTO.getUsername());
        caregiver.setPassword(caregiverDTO.getPassword());
        caregiver.setName(caregiverDTO.getName());
        caregiver.setDateOfBirth(caregiverDTO.getDateOfBirth());
        caregiver.setGender(caregiverDTO.getGender());
        caregiver.setAddress(caregiverDTO.getAddress());
        caregiver.setPatient(caregiverDTO.getListPatients());
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was updated in db", caregiver.getId());
        return caregiver.getId();

    }

    public void delete(Integer caregiverId) {
        CaregiverDetailsDTO caregiverDTO = this.findCaregiverById(caregiverId);
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        Set<Patient> patientSet = caregiver.getPatientList();
        if (patientSet != null) {
            for (Patient patient : patientSet) {
                patient.setCaregiver(null);
            }
        }
        caregiverRepository.deleteById(caregiverId);
    }

}