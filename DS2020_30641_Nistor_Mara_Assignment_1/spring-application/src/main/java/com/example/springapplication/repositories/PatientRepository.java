package com.example.springapplication.repositories;

import com.example.springapplication.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Patient> findByName(String name);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "WHERE p.id = :id ")
    Optional<Patient> readPatientById(@Param("id") Integer id);


}
