package com.example.springapplication.dtos.builders;

import com.example.springapplication.dtos.MedicationDTO;
import com.example.springapplication.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO toMedicationDTO(Medication medication) {
        return new MedicationDTO(medication.getId(), medication.getName(), medication.getEffects(), medication.getDosage());
    }


    public static Medication toEntity(MedicationDTO medicationDTO) {
        return new Medication(medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getEffects(),
                medicationDTO.getDosage()
        );
    }
}
