package com.example.springapplication.dtos;

import com.example.springapplication.entities.Patient;
import org.springframework.hateoas.RepresentationModel;

import java.sql.Date;
import java.util.Set;

public class CaregiverDetailsDTO extends RepresentationModel<CaregiverDTO> {

    private Integer id;
    private String name;
    private Date dateOfBirth;
    private String gender;
    private String address;
    private String username;
    private String password;
    private Set<Patient> listPatients;

    public CaregiverDetailsDTO() {

    }


    public CaregiverDetailsDTO(Integer id, String name, Date dateOfBirth, String gender, String address, Set<Patient> listPatients, String username, String password) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.gender = gender;
        this.listPatients = listPatients;
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public Set<Patient> getListPatients() {
        return listPatients;
    }

    public void setListPatients(Set<Patient> listPatients) {
        this.listPatients = listPatients;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
