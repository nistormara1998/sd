package com.example.springapplication.services;

import com.example.springapplication.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.example.springapplication.dtos.PatientDetailsDTO;
import com.example.springapplication.dtos.builders.PatientBuilder;
import com.example.springapplication.entities.Caregiver;
import com.example.springapplication.entities.MedicationPlan;
import com.example.springapplication.entities.Patient;
import com.example.springapplication.repositories.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public Patient assignPatient(Caregiver caregiver, Patient patient) {
        patient.setCaregiver(caregiver);
        patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was assigned to a caregiver", patient.getId());
        return patient;
    }


    public List<PatientDetailsDTO> findPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDetailsDTO)
                .collect(Collectors.toList());
    }

    public PatientDetailsDTO findPatientById(Integer id) {
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if (!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDetailsDTO(patientOptional.get());
    }


    public Integer insert(PatientDetailsDTO patientDTO) {
        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public Integer update(Integer id, PatientDetailsDTO patientDTO) {

        Optional<Patient> patientOptional = patientRepository.findById(id);
        if (!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        Patient patient = patientOptional.get();
        patient.setUsername(patientDTO.getUsername());
        patient.setPassword(patientDTO.getPassword());
        patient.setName(patientDTO.getName());
        patient.setDateOfBirth(patientDTO.getDateOfBirth());
        patient.setGender(patientDTO.getGender());
        patient.setAddress(patientDTO.getAddress());
        patient.setMedicalRecord(patientDTO.getMedicalRecord());
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was updated in db", patient.getId());
        return patient.getId();

    }

    public void delete(Integer patientId) {
        PatientDetailsDTO patientDTO = this.findPatientById(patientId);
        Patient patient = PatientBuilder.toEntity(patientDTO);
        MedicationPlan medicationPlan = patient.getMedicationPlan();
        if (medicationPlan != null) {
            medicationPlan.setPatient(null);
        }
        patientRepository.deleteById(patientId);
    }
}
