package com.example.springapplication.controllers;

import com.example.springapplication.dtos.CaregiverDTO;
import com.example.springapplication.dtos.CaregiverDetailsDTO;
import com.example.springapplication.services.CaregiverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "rest/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;
    private static final String INSERT_CAREGIVER = "/insert";
    private static final String UPDATE_CAREGIVER = "/update/{id}";
    private static final String DELETE_CAREGIVER = "/delete/{id}";
    private static final String GET_CAREGIVER = "/{id}";

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    private final Logger log = LoggerFactory.getLogger(CaregiverController.class);

    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<List<CaregiverDetailsDTO>> getCaregivers() {
        List<CaregiverDetailsDTO> dtos = caregiverService.findCaregivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = INSERT_CAREGIVER,
            method = RequestMethod.POST,
            headers = {"Content-Type=application/json", "Accept=application/json"})
    public ResponseEntity<Integer> insertCaregiver(@Valid @RequestBody CaregiverDetailsDTO caregiverDTO) {
        log.info("~ Request to insert caregiver ~");
        Integer caregiverId = caregiverService.insert(caregiverDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.CREATED);
    }


    @RequestMapping(
            value = GET_CAREGIVER,
            method = RequestMethod.GET
            //headers = {"Content-Type=application/json", "Accept=application/json"}
    )
    public ResponseEntity<CaregiverDetailsDTO> getCaregiver(@PathVariable("id") Integer caregiverId) {
        log.info("~ Request to get all caregivers ~");
        CaregiverDetailsDTO dto = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(
            value = UPDATE_CAREGIVER,
            method = RequestMethod.PUT)
    public ResponseEntity<Integer> updateCaregiver(@PathVariable("id") Integer id, @Valid @RequestBody CaregiverDetailsDTO caregiverDTO) {
        Integer caregiverId = caregiverService.update(id, caregiverDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.OK);
    }

    @RequestMapping(
            value = DELETE_CAREGIVER,
            method = RequestMethod.DELETE
    )
    public ResponseEntity<?> deleteCaregiverById(@PathVariable("id") Integer caregiverId) {
        log.info("~ Request to delete caregiver ~");
        caregiverService.delete(caregiverId);
        return ResponseEntity.ok().build();
    }
}
