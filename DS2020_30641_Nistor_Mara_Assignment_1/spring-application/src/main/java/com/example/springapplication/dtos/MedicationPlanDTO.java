package com.example.springapplication.dtos;

import com.example.springapplication.entities.Medication;
import com.example.springapplication.entities.Patient;
import org.springframework.hateoas.RepresentationModel;

import java.util.Set;

public class MedicationPlanDTO extends RepresentationModel<MedicationPlanDTO> {

    private Integer id;
    private String intake;
    private String period;
    private Set<Medication> medicationList;

    public MedicationPlanDTO() {

    }

    public MedicationPlanDTO(Integer id, String intake, String period, Set<Medication> medicationList) {
        this.id = id;
        this.intake = intake;
        this.period = period;
        this.medicationList = medicationList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Set<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(Set<Medication> medicationList) {
        this.medicationList = medicationList;
    }

}
