package com.example.springapplication.dtos;

import com.example.springapplication.entities.MedicationPlan;
import com.sun.istack.NotNull;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Column;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

public class PatientDetailsDTO extends RepresentationModel<PatientDetailsDTO> {

    @NotNull
    private Integer id;
    @NotNull
    private String name;
    @NotNull
    private Date dateOfBirth;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    @NotNull
    private String medicalRecord;
    @NotNull
    private String username;
    @NotNull
    private String password;

    private MedicationPlan medicationPlan;



    public PatientDetailsDTO(Integer id, String name, Date dateOfBirth, String gender, String address, String medicalRecord, String username, String password,MedicationPlan medicationPlan) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.username = username;
        this.password = password;
        this.medicationPlan = medicationPlan;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
