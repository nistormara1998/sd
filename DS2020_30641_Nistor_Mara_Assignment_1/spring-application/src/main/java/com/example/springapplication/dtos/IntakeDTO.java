package com.example.springapplication.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.sql.Date;

public class IntakeDTO extends RepresentationModel<IntakeDTO> {

    private Integer id;
    private Date startDate;
    private Date endDate;
    private MedicationDTO medicationDTO;
    private PatientDTO patientDTO;

    public IntakeDTO() {

    }

//    public IntakeDTO(Integer id, Date startDate, Date endDate, MedicationDTO medicationDTO, PatientDTO patientDTO) {
//        this.id = id;
//        this.startDate = startDate;
//        this.endDate = endDate;
//        this.medicationDTO = medicationDTO;
//        this.patientDTO = patientDTO;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public MedicationDTO getMedicationDTO() {
        return medicationDTO;
    }

    public void setMedicationDTO(MedicationDTO medicationDTO) {
        this.medicationDTO = medicationDTO;
    }

    public PatientDTO getPatientDTO() {
        return patientDTO;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patientDTO = patientDTO;
    }
}

