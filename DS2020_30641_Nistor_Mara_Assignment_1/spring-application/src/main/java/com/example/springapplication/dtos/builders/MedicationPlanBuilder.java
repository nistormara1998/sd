package com.example.springapplication.dtos.builders;

import com.example.springapplication.dtos.MedicationDTO;
import com.example.springapplication.dtos.MedicationPlanDTO;
import com.example.springapplication.entities.Medication;
import com.example.springapplication.entities.MedicationPlan;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(), medicationPlan.getIntake(), medicationPlan.getPeriod(),medicationPlan.getMedicationList());
    }


    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(medicationPlanDTO.getId(),
                medicationPlanDTO.getMedicationList(),
                medicationPlanDTO.getIntake(),
                medicationPlanDTO.getPeriod()

        );
    }
}
