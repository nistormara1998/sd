package com.example.springapplication.dtos.builders;

import com.example.springapplication.dtos.CaregiverDTO;
import com.example.springapplication.dtos.CaregiverDetailsDTO;
import com.example.springapplication.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getName(), caregiver.getDateOfBirth());
    }

    public static CaregiverDetailsDTO toCaregiverDetailsDTO(Caregiver caregiver) {
        return new CaregiverDetailsDTO(caregiver.getId(), caregiver.getName(), caregiver.getDateOfBirth(), caregiver.getGender(), caregiver.getAddress(), caregiver.getPatientList(), caregiver.getUsername(), caregiver.getPassword());
    }

    public static Caregiver toEntity(CaregiverDetailsDTO caregiverDetailsDTO) {
        return new Caregiver(caregiverDetailsDTO.getId(),
                caregiverDetailsDTO.getName(),
                caregiverDetailsDTO.getDateOfBirth(),
                caregiverDetailsDTO.getGender(),
                caregiverDetailsDTO.getAddress(),
                caregiverDetailsDTO.getListPatients(),
                caregiverDetailsDTO.getUsername(),
                caregiverDetailsDTO.getPassword()
        );
    }
}
