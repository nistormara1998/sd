package com.example.springapplication.dtos;

import org.springframework.hateoas.RepresentationModel;

public class MedicationDTO extends RepresentationModel<MedicationDTO> {

    private Integer id;
    private String name;
    private String effects;
    private String dosage;

    public MedicationDTO() {

    }

    public MedicationDTO(Integer id, String name, String effects, String dosage) {
        this.id = id;
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffects() {
        return effects;
    }

    public void setEffects(String effects) {
        this.effects = effects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
