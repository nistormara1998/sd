package com.example.springapplication.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.sql.Date;

public class CaregiverDTO extends RepresentationModel<CaregiverDTO> {

    private Integer id;
    private String name;
    private Date dateOfBirth;

    public CaregiverDTO() {

    }

    public CaregiverDTO(Integer id, String name, Date dateOfBirth) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}
