package com.example.springapplication.dtos.builders;

import com.example.springapplication.dtos.PatientDTO;
import com.example.springapplication.dtos.PatientDetailsDTO;
import com.example.springapplication.entities.Patient;

public class PatientBuilder {

    private PatientBuilder() {
    }
    

    public static PatientDetailsDTO toPatientDetailsDTO(Patient patient) {
        return new PatientDetailsDTO(patient.getId(), patient.getName(), patient.getDateOfBirth(), patient.getGender(),patient.getAddress(),patient.getMedicalRecord(),patient.getUsername(),patient.getPassword(),patient.getMedicationPlan());
    }

    public static Patient toEntity(PatientDetailsDTO patientDetailsDTO) {
        return new Patient(patientDetailsDTO.getId(),
                patientDetailsDTO.getName(),
                patientDetailsDTO.getDateOfBirth(),
                patientDetailsDTO.getGender(),
                patientDetailsDTO.getAddress(),
                patientDetailsDTO.getMedicalRecord(),
                patientDetailsDTO.getUsername(),
                patientDetailsDTO.getPassword(),
                patientDetailsDTO.getMedicationPlan()
                );
    }

}
