package com.example.springapplication.controllers;

import com.example.springapplication.dtos.CaregiverDetailsDTO;
import com.example.springapplication.dtos.MedicationPlanDTO;
import com.example.springapplication.dtos.PatientDTO;
import com.example.springapplication.dtos.PatientDetailsDTO;
import com.example.springapplication.dtos.builders.CaregiverBuilder;
import com.example.springapplication.dtos.builders.MedicationPlanBuilder;
import com.example.springapplication.dtos.builders.PatientBuilder;
import com.example.springapplication.entities.Caregiver;
import com.example.springapplication.entities.MedicationPlan;
import com.example.springapplication.entities.Patient;
import com.example.springapplication.services.CaregiverService;
import com.example.springapplication.services.MedicationPlanService;
import com.example.springapplication.services.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "rest/patient")
public class PatientController {

    private final PatientService patientService;
    private final CaregiverService caregiverService;
    private final MedicationPlanService medicationPlanService;
    private static final String INSERT_PATIENT = "/insert";
    private static final String UPDATE_PATIENT = "/update/{id}";
    private static final String DELETE_PATIENT = "/delete/{id}";
    private static final String GET_PATIENT = "/{id}";
    private static final String ASSIGN_CAREGIVER = "/addToCaregiver/{patientId}/{caregiverId}";

    @Autowired
    public PatientController(PatientService patientService, CaregiverService caregiverService, MedicationPlanService medicationPlanService) {
        this.patientService = patientService;
        this.caregiverService = caregiverService;
        this.medicationPlanService = medicationPlanService;
    }


    private final Logger log = LoggerFactory.getLogger(PatientController.class);

    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<List<PatientDetailsDTO>> getPatients() throws ParseException {
        List<PatientDetailsDTO> dtos = patientService.findPatients();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = INSERT_PATIENT,
            method = RequestMethod.POST,
            headers = {"Content-Type=application/json", "Accept=application/json"})
    public ResponseEntity<Integer> insertPatient(@Valid @RequestBody PatientDetailsDTO patientDTO) {
        log.info("~ Request to insert patient ~");
        Integer patientId = patientService.insert(patientDTO);

        return new ResponseEntity<>(patientId, HttpStatus.CREATED);
    }

    @RequestMapping(
            value = ASSIGN_CAREGIVER,
            method = RequestMethod.PUT
    )
    public ResponseEntity<String> assignCaregiver(@PathVariable("patientId") Integer patientId, @PathVariable("caregiverId") Integer caregiverId) {
        PatientDetailsDTO patientDTO = patientService.findPatientById(patientId);
        CaregiverDetailsDTO caregiverDTO = caregiverService.findCaregiverById(caregiverId);
        Patient patient = PatientBuilder.toEntity(patientDTO);
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        patientService.assignPatient(caregiver, patient);
        return new ResponseEntity<>(patient.getName(), HttpStatus.CREATED);
    }


    @RequestMapping(
            value = GET_PATIENT,
            method = RequestMethod.GET
    )
    public ResponseEntity<PatientDetailsDTO> getPatient(@PathVariable("id") Integer patientId) {
        log.info("~ Request to delete patient ~");
        PatientDetailsDTO dto = patientService.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(
            value = UPDATE_PATIENT,
            method = RequestMethod.PUT)
    public ResponseEntity<Integer> updatePatient(@PathVariable("id") Integer id, @Valid @RequestBody PatientDetailsDTO patientDTO) {
        Integer patientId = patientService.update(id, patientDTO);
        return new ResponseEntity<>(patientId, HttpStatus.OK);
    }

    @RequestMapping(
            value = DELETE_PATIENT,
            method = RequestMethod.DELETE
    )
    public ResponseEntity<?> deletePatientById(@PathVariable("id") Integer patientId) {
        log.info("~ Request to delete patient ~");
        patientService.delete(patientId);
        return ResponseEntity.ok().build();
    }
}
