package com.example.springapplication.controllers;

import com.example.springapplication.dtos.*;
import com.example.springapplication.dtos.builders.CaregiverBuilder;
import com.example.springapplication.dtos.builders.MedicationBuilder;
import com.example.springapplication.dtos.builders.MedicationPlanBuilder;
import com.example.springapplication.dtos.builders.PatientBuilder;
import com.example.springapplication.entities.Caregiver;
import com.example.springapplication.entities.Medication;
import com.example.springapplication.entities.MedicationPlan;
import com.example.springapplication.entities.Patient;
import com.example.springapplication.services.MedicationPlanService;
import com.example.springapplication.services.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "rest/medicationplan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;
    private final PatientService patientService;
    private static final String INSERT_MEDICATION_PLAN = "/insert";
    private static final String GET_MEDICATION_PLAN = "/{id}";
    private static final String ASSIGN_MEDICATION_PLAN = "/addToPatient/{patientId}/{medicationPlanId}";

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService, PatientService patientService) {
        this.medicationPlanService = medicationPlanService;
        this.patientService = patientService;
    }

    private final Logger log = LoggerFactory.getLogger(MedicationPlanController.class);

    @RequestMapping(
            value = INSERT_MEDICATION_PLAN,
            method = RequestMethod.POST,
            headers = {"Content-Type=application/json", "Accept=application/json"})
    public ResponseEntity<Integer> insertMedicationPlan(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) {
        log.info("~ Request to insert medication ~");
        Integer medicationPlanId = medicationPlanService.insert(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlanId, HttpStatus.CREATED);
    }

    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans() throws ParseException {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlans();
        for (MedicationPlanDTO dto : dtos) {
            Link medicationPlanLink = linkTo(methodOn(MedicationPlanController.class)
                    .getMedicationPlan(dto.getId())).withRel("medicationPlanDetails");
            dto.add(medicationPlanLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = GET_MEDICATION_PLAN,
            method = RequestMethod.GET
            //headers = {"Content-Type=application/json", "Accept=application/json"}
    )
    public ResponseEntity<MedicationPlanDTO> getMedicationPlan(@PathVariable("id") Integer medicationPlanId) {
        MedicationPlanDTO dto = medicationPlanService.findMedicationPlanById(medicationPlanId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(
            value = ASSIGN_MEDICATION_PLAN,
            method = RequestMethod.PUT
    )
    public ResponseEntity<String> assignMedicationPlanToPatient(@PathVariable("patientId") Integer patientId, @PathVariable("medicationPlanId") Integer medicationPlanId) {
        PatientDetailsDTO patientDTO = patientService.findPatientById(patientId);
        MedicationPlanDTO medicationPlanDTO = medicationPlanService.findMedicationPlanById(medicationPlanId);
        Patient patient = PatientBuilder.toEntity(patientDTO);
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
        medicationPlanService.assignMedicationPlan(medicationPlan, patient);
        return new ResponseEntity<>(patient.getName(), HttpStatus.CREATED);
    }
}
