package com.example.springapplication.controllers;

import com.example.springapplication.dtos.MedicationDTO;
import com.example.springapplication.dtos.MedicationPlanDTO;
import com.example.springapplication.dtos.builders.MedicationBuilder;
import com.example.springapplication.dtos.builders.MedicationPlanBuilder;
import com.example.springapplication.entities.Medication;
import com.example.springapplication.entities.MedicationPlan;
import com.example.springapplication.services.MedicationPlanService;
import com.example.springapplication.services.MedicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "rest/medication")
public class MedicationController {

    private final MedicationService medicationService;
    private final MedicationPlanService medicationPlanService;
    private static final String INSERT_MEDICATION = "/insert";
    private static final String UPDATE_MEDICATION = "/update/{id}";
    private static final String DELETE_MEDICATION = "/delete/{id}";
    private static final String GET_MEDICATION = "/{id}";
    private static final String ASSIGN_MEDICATION = "/addToMedicationList/{medicationId}/{medicationPlanId}";

    @Autowired
    public MedicationController(MedicationService medicationService, MedicationPlanService medicationPlanService) {
        this.medicationService = medicationService;
        this.medicationPlanService = medicationPlanService;
    }

    private final Logger log = LoggerFactory.getLogger(MedicationController.class);

    @RequestMapping(
            value = ASSIGN_MEDICATION,
            method = RequestMethod.PUT
    )
    public ResponseEntity<String> assignMedicationPlan(@PathVariable("medicationId") Integer medicationId, @PathVariable("medicationPlanId") Integer medicationPlanId) {
        MedicationDTO medicationDTO = medicationService.findMedicationById(medicationId);
        MedicationPlanDTO medicationPlanDTO = medicationPlanService.findMedicationPlanById(medicationPlanId);
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
        medicationService.assignMedicationToMedicationPlan(medicationPlan, medication);
        return new ResponseEntity<>(medication.getName(), HttpStatus.CREATED);
    }


    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<List<MedicationDTO>> getMedications() throws ParseException {
        List<MedicationDTO> dtos = medicationService.findMedication();
        for (MedicationDTO dto : dtos) {
            Link medicationLink = linkTo(methodOn(MedicationController.class)
                    .getMedication(dto.getId())).withRel("medicationDetails");
            dto.add(medicationLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = INSERT_MEDICATION,
            method = RequestMethod.POST,
            headers = {"Content-Type=application/json", "Accept=application/json"})
    public ResponseEntity<Integer> insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        log.info("~ Request to insert medication ~");
        Integer medicationId = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.CREATED);
    }


    @RequestMapping(
            value = GET_MEDICATION,
            method = RequestMethod.GET
            //headers = {"Content-Type=application/json", "Accept=application/json"}
    )
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable("id") Integer medicationId) {
        log.info("~ Request to delete medication ~");
        MedicationDTO dto = medicationService.findMedicationById(medicationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(
            value = UPDATE_MEDICATION,
            method = RequestMethod.PUT)
    public ResponseEntity<Integer> updateMedication(@PathVariable("id") Integer id, @Valid @RequestBody MedicationDTO medicationDTO) {
        Integer medicationId = medicationService.update(id, medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.OK);
    }

    @RequestMapping(
            value = DELETE_MEDICATION,
            method = RequestMethod.DELETE
    )
    public ResponseEntity<?> deleteMedicationById(@PathVariable("id") Integer medicationId) {
        log.info("~ Request to delete medication ~");
        medicationService.delete(medicationId);
        return ResponseEntity.ok().build();
    }
}
