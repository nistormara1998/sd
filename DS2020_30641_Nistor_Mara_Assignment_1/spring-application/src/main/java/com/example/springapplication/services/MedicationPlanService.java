package com.example.springapplication.services;

import com.example.springapplication.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.example.springapplication.dtos.MedicationPlanDTO;
import com.example.springapplication.dtos.builders.MedicationPlanBuilder;
import com.example.springapplication.entities.MedicationPlan;
import com.example.springapplication.entities.Patient;
import com.example.springapplication.repositories.MedicationPlanRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public List<MedicationPlanDTO> findMedicationPlans() {
        List<MedicationPlan> medicationPlanList = medicationPlanRepository.findAll();
        return medicationPlanList.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public MedicationPlanDTO findMedicationPlanById(Integer id) {
        Optional<MedicationPlan> medicationPlanOptional = medicationPlanRepository.findById(id);
        if (!medicationPlanOptional.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
        }
        return MedicationPlanBuilder.toMedicationPlanDTO(medicationPlanOptional.get());
    }

    public Integer insert(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        LOGGER.debug("Medication plan with id {} was inserted in db", medicationPlan.getId());
        return medicationPlan.getId();
    }

    public MedicationPlan assignMedicationPlan(MedicationPlan medicationPlan,Patient patient ) {
        medicationPlan.setPatient(patient);
        medicationPlanRepository.save(medicationPlan);
        LOGGER.debug("Medication plan with id {} was assigned to a patient", medicationPlan.getId());
        return medicationPlan;
    }
}
