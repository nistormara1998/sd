package com.example.springapplication.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.sql.Date;
import java.util.Objects;

public class PatientDTO extends RepresentationModel<PatientDTO> {

    private Integer id;
    private String name;
    private Date dateOfBirth;

    public PatientDTO() {

    }

    public PatientDTO(Integer id, String name, Date dateOfBirth) {

        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }


    @Override
    public int hashCode() {
        return Objects.hash(id,name, dateOfBirth);
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}
