package com.example.springapplication.repositories;

import com.example.springapplication.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {
}
